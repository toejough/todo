"""
Date Adapter.

Implements date/time interactions needed by todos.
"""


# [ Imports:Python ]
import datetime

# [ Imports:Project ]
import entities


# [ Internals ]
def _get_time():
    now = datetime.datetime.now()
    time = entities.Time(hour=now.hour, minute=now.minute, second=now.second)
    return time


# [ API ]
def get_weekday(date: entities.Date) -> entities.Weekdays:
    datetime_date = datetime.date(date.year, date.month, date.day)
    day_of_week = datetime_date.weekday()
    weekday_list = [
        entities.Weekdays.MONDAY,
        entities.Weekdays.TUESDAY,
        entities.Weekdays.WEDNESDAY,
        entities.Weekdays.THURSDAY,
        entities.Weekdays.FRIDAY,
        entities.Weekdays.SATURDAY,
        entities.Weekdays.SUNDAY,
    ]
    weekday = weekday_list[day_of_week]
    return weekday


def get_date():
    today = datetime.date.today()
    date = entities.Date(year=today.year, month=today.month, day=today.day)
    return date


def add_days(today: entities.Date, *, days: int) -> entities.Date:
    datetime_date = datetime.date(year=today.year, month=today.month, day=today.day)
    new_day = datetime_date + datetime.timedelta(days=days)
    date = entities.Date(year=new_day.year, month=new_day.month, day=new_day.day)
    return date


def get_datetime():
    date = get_date()
    time = _get_time()
    return entities.DateTime(date=date, time=time)
