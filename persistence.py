"""API for todos."""


# [ Imports ]
from __future__ import annotations
import datetime
import pathlib
import toml
import shell
import pipe
import branches
import entities


# [ Internals ]
def _bool_to_string(bool_value, *, name):
    return name if bool_value else f"not-{name}"


def _enum_to_string(enum_value):
    return enum_value.name.lower().replace('_', '-')


def _date_to_string(date_value):
    now = datetime.datetime.now()
    if date_value.date() < now.date():
        return 'overdue'
    if date_value.date() == now.date():
        return 'today'
    if date_value.date() == (now + datetime.timedelta(days=1)).date():
        return 'tomorrow'
    if date_value.date() <= (now + datetime.timedelta(days=7)).date():
        return 'within-a-week'
    if date_value.date() <= (now + datetime.timedelta(days=14)).date():
        return 'within-two-weeks'
    if date_value.date() <= (now + datetime.timedelta(days=30)).date():
        return 'within-a-month'
    return 'later'


def _apply_unless_none(value, *, func):
    if value is None:
        return None
    return func(value)


def _sorting_key(todo: shell.Todo):
    return (
        todo.completion_state.value,
        2 if todo.blocked_by_other else 1,
        1 if todo.important else 2,
        datetime_to_semantic_date(todo.due_date).value,
        1 if todo.blocking_other else 2,
        todo.expected_timespan.value,
        todo.role.value,
        -todo.created_at.timestamp(),
    )


# [ API ]
def _todos_to_data(todos):
    todo_dicts = []
    for this_todo in todos:
        as_dict = todo_as_dict(this_todo)
        todo_dicts.append(as_dict)
    return todo_dicts


def _data_to_todos(todos_data):
    todos = []
    for this_dict in todos_data:
        this_todo = todo_from_dict(this_dict)
        todos.append(this_todo)
    return todos


def store_active_filter(state, *, location_string):
    path = pathlib.Path(location_string)
    path = path.expanduser()
    state_str = toml.dumps({'state': list(state)})
    path.write_text(state_str)


def load_active_filter(path_string):
    path = pathlib.Path(path_string)
    path = path.expanduser()
    try:
        state_str = path.read_text()
    except FileNotFoundError:
        state = {'state': ['open']}
    else:
        state = toml.loads(state_str)

    return state['state']


def store_active_index(index, *, location_string):
    path = pathlib.Path(location_string)
    path = path.expanduser()
    state_str = toml.dumps({'index': index})
    path.write_text(state_str)


def load_active_index(path_string):
    path = pathlib.Path(path_string)
    path = path.expanduser()
    try:
        state_str = path.read_text()
    except FileNotFoundError:
        # XXX this logic is a shell decision (what do we want the default to be)
        #     not a persistence plugin decision
        state = {'index': 0}
    else:
        state = toml.loads(state_str)

    return state['index']


def _save(todos, *, path_string="~/todos.toml"):
    todos_text = toml.dumps({'todos': todos})
    path = pathlib.Path(path_string)
    path = path.expanduser()
    path.write_text(todos_text)


def _load(path_string="~/todos.toml"):
    path = pathlib.Path(path_string)
    path = path.expanduser()
    todos_text = path.read_text()
    todos = toml.loads(todos_text)
    return todos['todos']


def load_todos(path_string):
    todo_data = _load(path_string)
    todos = _data_to_todos(todo_data)
    return todos


def store_todos(todos, *, location_string):
    todo_data = _todos_to_data(todos)
    _save(todo_data, path_string=location_string)


def _date_to_dict(date):
    return {
        'year': date.year,
        'month': date.month,
        'day': date.day,
    }


def _time_to_dict(time):
    return {
        'hour': time.hour,
        'minute': time.minute,
        'second': time.second,
    }


def _datetime_to_dict(datetime):
    return {
        'date': _date_to_dict(datetime.date),
        'time': _time_to_dict(datetime.time),
    }


def todo_as_dict(todo):
    due_date = todo.due_date
    if isinstance(due_date, entities.Date):
        due_date = datetime.datetime(year=due_date.year, month=due_date.month, day=due_date.day)

    return {
        'type': 'todos:Todo',
        'work': todo.work,
        'created_at': _apply_unless_none(todo.created_at, func=datetime.datetime.isoformat),
        'important': todo.important.name,
        'blocking_other': todo.blocking_other,
        'blocked_by_other': todo.blocked_by_other,
        'role': todo.role.name,
        'expected_timespan': todo.expected_timespan.name,
        'completion_state': todo.completion_state.name,
        'crossed_out_at': _apply_unless_none(todo.crossed_out_at, func=_datetime_to_dict),
        'cancellation_reason': todo.cancellation_reason,
        'due_date': _apply_unless_none(due_date, func=datetime.datetime.isoformat),
        'scheduling': todo.scheduling.name,
    }


def _todo_from_data(data):
    return entities.DateTime(
        date=entities.Date(**data['date']),
        time=entities.Time(**data['time']),
    )


_p_apply_unless_none = pipe.Pipe(_apply_unless_none)
_p_if_not = pipe.Pipe(shell._if_not)


def _data_to_importance(data):
    tf_map = {
        True: entities.Importance.SHOULD,
        False: entities.Importance.WOULD_LIKE,
    }
    str_map = entities.Importance.__members__
    return tf_map.get(data, str_map.get(data, None))


def todo_from_dict(data, *, todo_type=shell.Todo):
    return todo_type(
        work=data['work'],
        created_at=datetime.datetime.fromisoformat(data['created_at']),
        important=_data_to_importance(data['important']),
        blocking_other=data.get('blocking_other', False),
        blocked_by_other=data.get('blocked_by_other', False),
        role=shell.Roles[data.get('role', "QE_CONTRIBUTOR")],
        expected_timespan=shell.Timespans[data['expected_timespan']],
        completion_state=shell.CompletionState[data['completion_state']],
        crossed_out_at=_apply_unless_none(data.get('crossed_out_at', None), func=_todo_from_data),
        cancellation_reason=data.get('cancellation_reason', ''),
        due_date=branches.do_if_none(_apply_unless_none(
            data.get('due_date', None), func=datetime.datetime.fromisoformat
        ), func=datetime.datetime.now),
        scheduling=data.get('scheduling', None) | _p_apply_unless_none(func=lambda s: shell.Scheduling[s]) | _p_if_not(default=shell.Scheduling.ONCE),
    )
