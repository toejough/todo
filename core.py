"""API for todos."""


# [ Imports ]
from __future__ import annotations
import din
import contextlib
import inspect
import datetime
import enum
import typing
import pathlib
import toml
import pipe
import entities
import use_cases


# [ Internals ]
def _apply_unless_none(value, *, func):
    if value is None:
        return None
    return func(value)


def _do_if_none(value, *, func):
    if value is None:
        return func()
    return value


# [ API:Data ]
CompletionState = entities.CompletionStatus


class Timespans(enum.Enum):
    MINUTES = 1
    HOURS = 2
    DAYS = 3

Roles = entities.Roles


Scheduling = entities.Intervals


class Todo(din.EqualityMixin, din.ReprMixin, din.FrozenMixin):

    def __init__(
        self,
        work: str,
        *,
        created_at: datetime.datetime,
        # XXX update importance to an Importance enum: CRITICAL, NEED, WANT, IDEA
        important: bool,
        blocking_other: bool,
        blocked_by_other: bool,
        role: Roles,
        expected_timespan: Timespans,
        completion_state: CompletionState,
        crossed_out_at: typing.Optional[datetime.datetime],
        cancellation_reason: str,
        due_date: datetime.datetime,
        scheduling: Scheduling,
    ) -> None:
        super().__init__()
        self.work = work
        self.created_at = created_at
        self.important = important
        self.blocking_other = blocking_other
        self.blocked_by_other = blocked_by_other
        self.role = role
        self.expected_timespan = expected_timespan
        self.completion_state = completion_state
        self.crossed_out_at = crossed_out_at
        self.cancellation_reason = cancellation_reason
        self.due_date = due_date
        self.scheduling = scheduling


# [ API:Functions ]
def copy_with(todo, **kwargs):
    return todo.copy_with(**kwargs)


def build_new_todo(
    work, *,
    important=True,
    due_date=None,
    blocked_by_other=False,
    blocking_other=True,
    role=Roles.QE_CONTRIBUTOR,
    expected_timespan=Timespans.MINUTES,
    scheduling=Scheduling.ONCE,
    todo_type=Todo,
):
    return todo_type(
        work=work,
        created_at=datetime.datetime.now(),
        completion_state=CompletionState.OPEN,
        important=important,
        due_date=_do_if_none(due_date, func=datetime.datetime.now),
        blocked_by_other=blocked_by_other,
        blocking_other=blocking_other,
        role=role,
        expected_timespan=expected_timespan,
        crossed_out_at=None,
        cancellation_reason='',
        scheduling=scheduling,
    )


def cancel_todo(todo, *, reason, cancellation_time):
    todo = use_cases.complete_todo(todo)
    return todo.copy_with(
        completion_state=CompletionState.CANCELLED,
        cancellation_reason=reason,
    )


def reschedule_todo(todo, *, now):
    if todo.scheduling is Scheduling.DAILY:
        return todo.copy_with(due_date=now + datetime.timedelta(days=1))
    if todo.scheduling is Scheduling.WEEKLY:
        return todo.copy_with(due_date=now + datetime.timedelta(days=7))
    if todo.scheduling is Scheduling.MONTHLY:
        return todo.copy_with(due_date=now + datetime.timedelta(days=30))
    if todo.scheduling is Scheduling.QUARTERLY:
        return todo.copy_with(due_date=now + datetime.timedelta(days=3*30))
    if todo.scheduling is Scheduling.ANNUALLY:
        return todo.copy_with(due_date=now + datetime.timedelta(days=365))


@pipe.Pipe
def _p_from_ordinal(ordinal):
    return datetime.datetime.fromordinal(ordinal)


def _to_datetime(internal_datetime):
    return datetime.datetime(
        year=internal_datetime.date.year,
        month=internal_datetime.date.month,
        day=internal_datetime.date.day,
        hour=internal_datetime.time.hour,
        minute=internal_datetime.time.minute,
        second=internal_datetime.time.second,
    )


def reschedule_upcoming_todos(original_todos, *, now):
    new_todos = []
    for this_todo in original_todos:
        if this_todo.scheduling is Scheduling.ONCE or this_todo.completion_state is CompletionState.OPEN:
            new_todos.append(this_todo)
        else:
            if this_todo.scheduling is Scheduling.DAILY:
                # reopen if midnight of the day after crossout is in the past
                # due date is one day after crossout
                days_till_reopen = 1
                days_till_due_again = 1
            elif this_todo.scheduling is Scheduling.WEEKLY:
                # reopen if midnight of three days after crossout is in the past
                # due date is 7 days after crossout
                days_till_reopen = 3
                days_till_due_again = 7
            elif this_todo.scheduling is Scheduling.MONTHLY:
                # reopen if midnight of 15 days after crossout is in the past
                # due date is 30 days after crossout
                days_till_reopen = 15
                days_till_due_again = 30
            elif this_todo.scheduling is Scheduling.QUARTERLY:
                # reopen if midnight of 45 days after crossout is in the past
                # due date is 90 days after crossout
                days_till_reopen = 45
                days_till_due_again = 90
            elif this_todo.scheduling is Scheduling.ANNUALLY:
                # reopen if midnight of 180 days after crossout is in the past
                # due date is 365 days after crossout
                days_till_reopen = 180
                days_till_due_again = 365
            reopen_date = (_to_datetime(this_todo.crossed_out_at) + datetime.timedelta(days=days_till_reopen)).date().toordinal() | _p_from_ordinal
            if reopen_date < now:
                due_date = (_to_datetime(this_todo.crossed_out_at) + datetime.timedelta(days=days_till_due_again))
                reopened_todo = update_completion_state(this_todo, completion_state=CompletionState.OPEN, now=now, cancellation_reason='')
                rescheduled_todo = reopened_todo.copy_with(due_date=due_date)
                new_todos.append(rescheduled_todo)
            else:
                new_todos.append(this_todo)
    return new_todos


def update_completion_state(todo, *, completion_state, now, cancellation_reason):
    # cancelling
    if completion_state is CompletionState.CANCELLED:
        return cancel_todo(todo, reason=cancellation_reason, cancellation_time=now)
    # updating cancellation reason
    if completion_state is None and todo.completion_state is CompletionState.CANCELLED:
        return todo.copy_with(cancellation_reason=cancellation_reason)
    # not cancelling or updating a cancelled todo, but supplied a cancellation reason - error
    if cancellation_reason:
        raise RuntimeError("Cancellation reason supplied on non-cancellation update to a non-cancelled todo!")
    # completing
    if completion_state is CompletionState.DONE:
        return use_cases.complete_todo(todo)
    # re-opening
    if completion_state is CompletionState.OPEN:
        return todo.copy_with(completion_state=CompletionState.OPEN, cancellation_reason='', crossed_out_at=None)
    # not modifying
    return todo
