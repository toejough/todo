#! /usr/bin/env python


# [ Imports ]
from __future__ import annotations
import datetime
import click
import click_repl
import click_default_group
from prompt_toolkit import history
import shell
import pathlib
import branches
import pipe
import functools
import din
import sys
import bpdb
import enum
import entities
import use_cases

sys.breakpointhook = bpdb.set_trace


# [ Internals ]
# XXX Abstract out state conversion to strings
# XXX Abstract out state types to enums
def _get_state_strings():
    return (
        *(_enum_to_string(r) for r in shell.Roles),
        *(_enum_to_string(i) for i in entities.Importance),
        *("blocked", "not-blocked"),
        *("blocking", "not-blocking"),
        *(_enum_to_string(t) for t in shell.Timespans),
        *(_enum_to_string(s) for s in shell.CompletionState),
        *(_enum_to_string(d) for d in shell.SemanticDates),
        *(_enum_to_string(s) for s in shell.Scheduling),
        *(_enum_to_string(w) for w in entities.Weekdays),
    )


def _update(todo_list, *, state, note):
    # get data to filter_list by
    remaining_state = state
    # get actual attribute states from text list
    timespans, remaining_state = _apply_or_split(
        remaining_state,
        func=_str_to_expected_timespan_or_none,
    )
    blocking_states, remaining_state = _apply_or_split(
        remaining_state,
        func=_str_to_blocking_or_none,
    )
    blocked_states, remaining_state = _apply_or_split(
        remaining_state,
        func=_str_to_blocked_or_none,
    )
    importance_states, remaining_state = _apply_or_split(
        remaining_state,
        func=_str_to_importance_or_none,
    )
    roles, remaining_state = _apply_or_split(
        remaining_state,
        func=_str_to_role_or_none,
    )
    due_dates, remaining_state = _apply_or_split(
        remaining_state,
        func=_str_to_due_date_or_none,
    )
    schedules, remaining_state = _apply_or_split(
        remaining_state,
        func=_str_to_schedule_or_none,
    )
    completion_states, remaining_state = _apply_or_split(
        remaining_state,
        func=_str_to_completion_state_or_none,
    )
    weekdays, remaining_state = _apply_or_split(
        remaining_state,
        func=_str_to_weekday_or_none,
    )

    yield from shell.update_list(
        todo_list,
        timespan         =_get_or_none(timespans, key=-1),
        blocking_state   =_get_or_none(blocking_states, key=-1),
        blocked_state    =_get_or_none(blocked_states, key=-1),
        importance_state =_get_or_none(importance_states, key=-1),
        role             =_get_or_none(roles, key=-1),
        due_date         =_get_or_none(due_dates, key=-1),
        scheduling       =_get_or_none(schedules, key=-1),
        completion_state =_get_or_none(completion_states, key=-1),
        note             =note,
        weekday          =_get_or_none(weekdays, key=-1),
    )


def _raw_stats():
    # load todos
    todos = shell.load_todos()
    # show number open
    print(f"Open: {len([t for t in todos if t.completion_state is shell.CompletionState.OPEN])}")
    # show number done
    print(f"Done: {len([t for t in todos if t.completion_state is shell.CompletionState.DONE])}")
    # show number cancelled
    print(f"Cancelled: {len([t for t in todos if t.completion_state is shell.CompletionState.CANCELLED])}")
    # show active
    try:
        active = _get_active()
    except RuntimeError:
        active = None
    print(f"Active: {_str(active)}")


def _get_active():
    # load todos
    todo_list = shell.load_todos()
    # load filter
    state = shell.load_active_filter()
    # load index
    active_index = shell.load_active_index()
    # filter todos
    matches = _filter_list_by_state(todo_list, state=state)
    if not matches:
        raise RuntimeError(f"No matches for active filter: [{'|'.join(state)}]")
    # get the todo at the index
    if len(matches) <= active_index:
        active_index = 0
        shell.store_active_index(active_index)
    return matches[active_index]


def _enum_to_string(enum_value):
    return enum_value.name.lower().replace('_', '-')


def _bool_to_string(bool_value, *, name):
    return name if bool_value else f"not-{name}"


def _date_to_string(date_value):
    if isinstance(date_value, entities.Date):
        date_value = datetime.datetime(year=date_value.year, month=date_value.month, day=date_value.day)
    # XXX could/should do date to semantic date, then enum to str
    now = datetime.datetime.now()
    if date_value.date() < now.date():
        return 'overdue'
    if date_value.date() == now.date():
        return 'today'
    if date_value.date() == (now + datetime.timedelta(days=1)).date():
        return 'tomorrow'
    if date_value.date() <= (now + datetime.timedelta(days=7)).date():
        return 'within-a-week'
    if date_value.date() <= (now + datetime.timedelta(days=14)).date():
        return 'within-two-weeks'
    if date_value.date() <= (now + datetime.timedelta(days=30)).date():
        return 'within-a-month'
    return 'later'


def _state(todo):
    return (
        _enum_to_string(todo.completion_state),
        _bool_to_string(todo.blocked_by_other, name='blocked'),
        _enum_to_string(todo.important),
        _date_to_string(todo.due_date),
        _bool_to_string(todo.blocking_other, name='blocking'),
        _enum_to_string(todo.expected_timespan),
        _enum_to_string(todo.role),
        _enum_to_string(todo.scheduling),
    )


def _str(todo):
    if not todo:
        return str(None)
    lines = [
        "Todo:",
        f"  summary: {todo.work}",
        f"  state: [{'|'.join(_state(todo))}]",
        f"  created: {todo.created_at}",
    ]
    if todo.cancellation_reason:
        lines.append(f"  cancellation reason: {todo.cancellation_reason}")
    if todo.crossed_out_at:
        lines.append(f"  crossed out at: {todo.crossed_out_at}")
    return "\n".join(lines)


def _str_to_completion_state_or_none(string):
    value_map = {k.lower().replace('_', '-'): v for k, v in shell.CompletionState.__members__.items()}
    return value_map.get(string, None)


def _str_to_importance_or_none(string):
    old_value_map = {
        'important': entities.Importance.SHOULD,
        'unimportant': entities.Importance.WOULD_LIKE,
    }
    value_map = {
        k.lower().replace('_', '-'): v for k, v in entities.Importance.__members__.items()
    }

    return old_value_map.get(string, value_map.get(string, None))


def _str_to_due_date_or_none(string):
    semantic_date_map = {k.lower().replace('_', '-'): v for k, v in shell.SemanticDates.__members__.items()}
    return semantic_date_map.get(string, None)


def _str_to_weekday_or_none(string):
    weekday_map = {k.lower().replace('_', '-'): v for k, v in entities.Weekdays.__members__.items()}
    return weekday_map.get(string, None)


def _str_to_schedule_or_none(string):
    value_map = {k.lower().replace('_', '-'): v for k, v in shell.Scheduling.__members__.items()}
    return value_map.get(string, None)


def _str_to_completion_state_or_none(string):
    value_map = {k.lower().replace('_', '-'): v for k, v in shell.CompletionState.__members__.items()}
    return value_map.get(string, None)


def _str_to_blocked_or_none(string):
    value_map = {'blocked': True, 'not-blocked': False}
    return value_map.get(string, None)


def _str_to_blocking_or_none(string):
    value_map = {'blocking': True, 'not-blocking': False}
    return value_map.get(string, None)


def _str_to_role_or_none(string):
    value_map = {k.lower().replace('_', '-'): v for k, v in shell.Roles.__members__.items()}
    return value_map.get(string, None)


def _role_strs_to_roles(role_strs):
    return [shell.Roles[rs.upper().replace('-', '_')] for rs in role_strs]


def _str_to_expected_timespan_or_none(string):
    value_map = {k.lower().replace('_', '-'): v for k, v in shell.Timespans.__members__.items()}
    return value_map.get(string, None)


def _apply_or_split(values, *, func):
    results = tuple()
    remaining_values = tuple()
    for this_value in values:
        this_result = func(this_value)
        if this_result is None:
            remaining_values = (*remaining_values, this_value)
        else:
            results = (*results, this_result)
    return results, remaining_values


def _get_or_none(thing, *, key):
    try:
        return thing[key]
    except (IndexError, KeyError):
        return None


def _parse_snippet_or_none(string):
    if not string.startswith('snippet:'):
        return None
    return string.split('snippet:', 1)[-1]


def _filter_list_by_state(todo_list, *, state):
    shell.store_active_filter(state)
    # get data to filter_list by
    remaining_state = state
    # get actual attribute states from text list
    snippets, remaining_state = _apply_or_split(remaining_state, func=_parse_snippet_or_none)
    completion_states, remaining_state = _apply_or_split(
        remaining_state,
        func=_str_to_completion_state_or_none,
    )
    timespans, remaining_state = _apply_or_split(
        remaining_state,
        func=_str_to_expected_timespan_or_none,
    )
    blocking_states, remaining_state = _apply_or_split(
        remaining_state,
        func=_str_to_blocking_or_none,
    )
    blocked_states, remaining_state = _apply_or_split(
        remaining_state,
        func=_str_to_blocked_or_none,
    )
    importance_states, remaining_state = _apply_or_split(
        remaining_state,
        func=_str_to_importance_or_none,
    )
    due_dates, remaining_state = _apply_or_split(
        remaining_state,
        func=_str_to_due_date_or_none,
    )
    schedules, remaining_state = _apply_or_split(
        remaining_state,
        func=_str_to_schedule_or_none,
    )
    # XXX once all filterable values are their own types, we can:
    #     value_map = _apply_conversions(state, *converters)
    #     shell.filter_list(timespans=value_map.get(shell.Timespans, default=tuple(shell.Timespans)))
    # get the actual roles
    role_strs, remaining_state = _split_role_strs_out(remaining_state)
    roles = _role_strs_to_roles(role_strs)
    todo_list = use_cases.filter_to_roles(todo_list, *roles)

    # filter
    return shell.filter_list(
        todo_list,
        snippet           =shell._if_not(_get_or_none(snippets, key=0), default=''),
        completion_states =shell._if_not(completion_states,             default=tuple(shell.CompletionState)),
        timespans         =shell._if_not(timespans,                     default=tuple(shell.Timespans)),
        blocking_states   =shell._if_not(blocking_states,               default=(True, False)),
        blocked_states    =shell._if_not(blocked_states,                default=(True, False)),
        importance_states =shell._if_not(importance_states,             default=tuple(entities.Importance)),
        due_dates         =shell._if_not(due_dates,                     default=tuple(shell.SemanticDates)),
        schedules         =shell._if_not(schedules,                     default=tuple(shell.Scheduling)),
    )

    # XXX - need some checking for unmatched state


# [ Internal:Decisions ]
class Sentinels(enum.Enum):
    NOT_SET = enum.auto()


class Switch:
    def __init__(self, mapping, *, default=Sentinels.NOT_SET):
        self._mapping = mapping
        self.default = default

    def __getitem__(self, key):
        if key in self._mapping:
            return self._mapping[key]
        elif self.default is not Sentinels.NOT_SET:
            return self.default
        else:
            raise RuntimeError(f"Specified key ({key}) not found in the provided mapping (keys: {self._mapping.keys()}), and no default was supplied.")


class GetCommandResponse(enum.Enum):
    RETURN_EXACT = enum.auto()
    RETURN_PARTIAL = enum.auto()


class GetPartialCommandResponse(enum.Enum):
    RETURN_NONE = enum.auto()
    RETURN_COMMAND = enum.auto()
    RAISE = enum.auto()


def _get_get_command_response(exact_match_exists):
    if exact_match_exists:
        response = GetCommandResponse.RETURN_EXACT
    else:
        response = GetCommandResponse.RETURN_PARTIAL
    return response


def _get_get_partial_command_response(num_partial_matches):
    if num_partial_matches == 0:
        response = GetPartialCommandResponse.RETURN_NONE
    elif num_partial_matches == 1:
        response = GetPartialCommandResponse.RETURN_COMMAND
    elif 1 < num_partial_matches:
        response = GetPartialCommandResponse.RAISE
    else:
        raise RuntimeError(f"Valid values for num_partial_matches are 0, 1, or higher.  Received {num_partial_matches}.")
    return response


class CliResponse(enum.Enum):
    DO_NOTHING = enum.auto()
    INVOKE_REPL = enum.auto()


def _get_cli_response(subcommand_was_invoked):
    if subcommand_was_invoked:
        response = CliResponse.DO_NOTHING
    else:
        response = CliResponse.INVOKE_REPL
    return response


def _get_partial_command(self, context, command_text):
    """
    Get the partial command.

    Specific Behaviors:
    * When no command name starts with the command_text, returns None.
    * When one command name starts with the command_text, returns that command.
    * When mulitple command names start with the command_text, raise an exception.
    """
    # Resolve decision criteria
    matching_command_names = tuple(c for c in self.list_commands(context) if c.startswith(command_text))
    # Decide on response
    response = _get_get_partial_command_response(len(matching_command_names))
    # Map response to action
    super_get_command = super(type(self), self).get_command
    actions = {
        GetPartialCommandResponse.RETURN_NONE: lambda: None,
        GetPartialCommandResponse.RETURN_COMMAND: lambda: super_get_command(context, matching_command_names[0]),
        GetPartialCommandResponse.RAISE: lambda: context.fail(f"Too many matches: {sorted(matching_command_names)}"),
    }
    # Take action
    return Switch(actions)[response]()


# [ Internal:Dependencies ]
class _AliasedGroup(click.Group):
    """
    Allow partial matches for commands.

    Examples:
    li -> list
    tom -> tomorrow

    Errors if too many matches:
    l -> Error: Too many matches: list, later
    """

    def get_command(self, context, command_text):
        """
        Get the command.

        Specific Behaviors:
        * When an exact command name match exists for command_text, returns that command.
        * else:
            * When no command name starts with the command_text, returns None.
            * When one command name starts with the command_text, returns that command.
            * When mulitple command names start with the command_text, raise an exception.
        """
        # XXX think about how this is piping primary-adapter-input | core-decision-making | secondary-adapter-output
        #     can we make that more explicit AND simpler?
        # Resolve decision criteria
        exact_match_exists = command_text in self.list_commands(context)
        # Decide on response
        response = _get_get_command_response(exact_match_exists)
        # Map response to action
        super_get_command = super().get_command
        actions = {
            GetCommandResponse.RETURN_EXACT: lambda: super_get_command(context, command_text),
            GetCommandResponse.RETURN_PARTIAL: lambda: _get_partial_command(self, context, command_text),
        }
        # Take action
        return Switch(actions)[response]()


# [ API ]
@click.group(cls=_AliasedGroup, invoke_without_command=True)
@click.pass_context
def cli(context):
    """
    Invoke the repl if no subcommands were invoked via the CLI.

    Specific behaviors:
    * When no subcommands are invoked, invoke the repl
    * When subcommands are invoked, do nothing
    """
    # Get decision criteria
    subcommand_was_invoked = context.invoked_subcommand
    # Decide on response
    response = _get_cli_response(subcommand_was_invoked)
    # Map response to action
    actions = {
        CliResponse.DO_NOTHING: lambda: None,
        CliResponse.INVOKE_REPL: lambda: context.invoke(repl),
    }
    # Take action
    return Switch(actions)[response]()


@cli.command()
@click.argument('summary')
@click.argument('state', nargs=-1, type=click.Choice(_get_state_strings()))
def add(summary, state):
    # defaults (not supported by click for variadic args)
    state = ('important', 'today', 'not-blocked', 'blocking', 'director', 'minutes', 'once', *state)
    # translate the state
    remaining_state = state
    importance_values, remaining_state = _apply_or_split(state, func=_str_to_importance_or_none)
    due_date_values, remaining_state = _apply_or_split(state, func=_str_to_due_date_or_none)
    blocked_values, remaining_state = _apply_or_split(state, func=_str_to_blocked_or_none)
    blocking_values, remaining_state = _apply_or_split(state, func=_str_to_blocking_or_none)
    role_values, remaining_state = _apply_or_split(state, func=_str_to_role_or_none)
    expected_timespan_values, remaining_state = _apply_or_split(
        state,
        func=_str_to_expected_timespan_or_none,
    )
    scheduling_values, remaining_state = _apply_or_split(state, func=_str_to_schedule_or_none)
    # build the new todo
    new_todo = shell.build_new_todo(
        summary,
        important=importance_values[-1],
        due_date=due_date_values[-1],
        blocked_by_other=blocked_values[-1],
        blocking_other=blocking_values[-1],
        role=role_values[-1],
        expected_timespan=expected_timespan_values[-1],
        scheduling=scheduling_values[-1],
    )
    # add it
    shell.add_to_list(new_todo)

    # print summary
    print(f"Added:\n{_str(new_todo)}\n")
    _raw_stats()
    print()


@cli.command()
def first():
    # load state
    state = shell.load_active_filter()
    # list todos according to that state
    todo_list = shell.load_todos()

    todos = _filter_list_by_state(todo_list, state=state)
    if not todos:
        raise click.ClickException("No todos match!")
    # update active index
    shell.store_active_index(0)
    first_todo = todos[0]

    # print summary
    print(_str(first_todo))
    print()



@cli.command()
def next():
    # load state
    state = shell.load_active_filter()
    # list todos according to that state
    todo_list = shell.load_todos()
    todos = _filter_list_by_state(todo_list, state=state)
    if not todos:
        raise click.ClickException("No todos match!")

    active_index = shell.load_active_index()
    index = min(active_index + 1, len(todos) - 1)
    if len(todos) - 1 == index:
        print("at the end of the list")
    # activate the todo
    next_todo = todos[index]
    shell.store_active_index(index)
    # return the used todo
    print(_str(next_todo))
    print()


@cli.command()
def previous():
    # load state
    state = shell.load_active_filter()
    # list todos according to that state
    todo_list = shell.load_todos()
    todos = _filter_list_by_state(todo_list, state=state)
    if not todos:
        raise click.ClickException("No todos match!")

    active_index = shell.load_active_index()
    index = max(active_index - 1, 0)
    if 0 == index:
        print("at the start of the list")
    # activate the todo
    previous_todo = todos[index]
    shell.store_active_index(index)
    print(_str(previous_todo))
    print()


@cli.command()
def which():
    try:
        active = _get_active()
    except RuntimeError as error:
        raise click.ClickException(error.args[0])
    active_search = shell.load_active_filter()

    # print summary
    print(f"Active Search: {active_search}")
    print(f"Active Todo:\n{_str(active)}")
    print()


def _enum_to_string(value):
    return value.name.lower().replace('_', '-')


def _get_positive_role_strs():
    return [r.name.lower().replace('_', '-') for r in shell.Roles]


def _get_negative_role_strs():
    positive_role_strs = _get_positive_role_strs()
    return [f'!{rs}' for rs in positive_role_strs]


def _split_role_strs_out(state):
    positive_role_strs = _get_positive_role_strs()
    negative_role_strs = _get_negative_role_strs()
    all_role_strs = positive_role_strs + negative_role_strs
    found_role_strs = []
    remaining_state = []
    for this_str in state:
        if this_str in all_role_strs:
            found_role_strs.append(this_str)
        else:
            remaining_state.append(this_str)
    return found_role_strs, remaining_state


def _filter_to_positive_strs(role_strs):
    positive_role_strs = _get_positive_role_strs()
    return [rs for rs in role_strs if rs in positive_role_strs]


def _is_negative_role_str(role_str):
    negative_role_strs = _get_negative_role_strs()
    return role_str in negative_role_strs


def _get_base_str(maybe_negative_str):
    if not maybe_negative_str.startswith('!'):
        positive_str = maybe_negative_str
    else:
        negative_str = maybe_negative_str
        positive_str = negative_str[1:]
    return positive_str


@cli.command('list')
@click.option('--limit', default=20)
@click.option('--snippet', default=None)
@click.argument('state', nargs=-1, type=click.Choice((*_get_state_strings(), *_get_negative_role_strs())))
def list_(limit, snippet, state):
    # default state (not allowed with click with variadic args)
    if not state:
        state = ('open',)
    # add default roles (if no positive roles specified, or first is negative)
    role_strs, state = _split_role_strs_out(state)
    if not role_strs or not _filter_to_positive_strs(role_strs) or _is_negative_role_str(role_strs[0]):
        role_strs = _get_positive_role_strs() + role_strs
    # reduce to last occurrence of each role (a, b, a -> b, a)
    remaining_role_strs, role_strs = role_strs, []
    while remaining_role_strs:
        first, *remaining_role_strs = remaining_role_strs
        if first not in remaining_role_strs:
            role_strs.append(first)
    # process pos/neg roles (b, -a, a, -b -> a)
    remaining_role_strs, role_strs = role_strs, []
    for this_role_str in remaining_role_strs:
        if _is_negative_role_str(this_role_str):
            role_str = _get_base_str(this_role_str)
            if role_str in role_strs:
                role_strs.remove(role_str)
        else:
            role_strs.append(this_role_str)
    state = (*state, *role_strs)
    # add snippet to the state
    if snippet:
        state = (*state, f"snippet:{snippet}")
    # load todos
    todo_list = shell.load_todos()
    print(f"{len(todo_list)} total todos")
    formatted_state = f"[{'|'.join(state)}]"
    # filter them
    print(f"Filtering by {formatted_state}...")
    todos = _filter_list_by_state(todo_list, state=state)
    # error handling
    if not todos:
        raise click.ClickException("No todos match!")
    # set the todo_index
    shell.store_active_index(0)
    # display the list
    items = [(t.work, _state(t)) for t in todos]
    reported_state = ()
    print(f"Todos:")
    for summary, todo_state in items[:limit]:
        if todo_state != reported_state:
            print(f"[{'|'.join((t for t in todo_state if t not in reported_state))}]")
            reported_state = todo_state
        print(f"  {summary}")
    if limit < len(items):
        print(f"...and {len(items) - limit} more...")
    print()
    # display stats
    _raw_stats()
    print()


@cli.command()
@click.option('--limit', default=20)
def relist(limit):
    # load state
    list_state = shell.load_active_filter()
    todo_list = shell.load_todos()
    # list todos according to that state
    todos = _filter_list_by_state(todo_list, state=list_state)
    if not todos:
        raise click.ClickException("No todos match!")

    items = [(t.work, _state(t)) for t in todos]

    # print summary
    reported_state = ()
    print(f"Todos:")
    for summary, todo_state in items[:limit]:
        if todo_state != reported_state:
            print(f"[{'|'.join((t for t in todo_state if t not in reported_state))}]")
            reported_state = todo_state
        print(f"  {summary}")
    if limit < len(items):
        print(f"...and {len(items) - limit} more...")
    print()
    _raw_stats()
    print()


@cli.command()
@click.argument('state', nargs=-1, type=click.Choice(_get_state_strings()))
@click.option('--note', default='')
def update(state, *, note):
    return _raw_update(state, note=note)


def _raw_update(state, *, note):
    # load state
    update_state = state
    # list todos according to that state
    todo_list = shell.load_todos()
    try:
        active = _get_active()
    except RuntimeError as e:
        # no matches
        raise click.ClickException(e.args[0])
    # make updates
    matches = [active]
    try:
        for old_thing, new_thing in zip(matches, _update(matches, state=update_state, note=note)):
            # remove old versions
            todo_list.remove(old_thing)
            # add new versions
            todo_list.append(new_thing)
            # print summary
            if any(s in update_state for s in ('done', 'cancelled')) and new_thing.completion_state == shell.CompletionState.OPEN:
                print(f"Rescheduled:\n{_str(new_thing)}\n")
            else:
                print(f"Updated:\n{_str(new_thing)}\n")
    except RuntimeError as e:
        raise click.ClickException(e.args[0])
    # sort
    todo_list = shell.sort_(todo_list)
    # save
    shell.store_todos(todo_list)

    # print summary
    list_state = shell.load_active_filter()
    matches = _filter_list_by_state(todo_list, state=list_state)
    if matches:
        active_index = shell.load_active_index()
        current_active_index = min(active_index, len(matches) - 1)
        shell.store_active_index(current_active_index)
    else:
        print(f"No todos match the filter anymore: [{'|'.join(list_state)}]")
    print()
    _raw_stats()
    print()


@cli.command('update-all')
@click.argument('state', nargs=-1, type=click.Choice(_get_state_strings()))
@click.option('--note', default='')
def update_all(state, *, note):
    # load state
    list_state = shell.load_active_filter()
    update_state = state
    # list todos according to that state
    todo_list = shell.load_todos()
    matches = _filter_list_by_state(todo_list, state=list_state)
    # else all issues
    if not matches:
        raise click.ClickException(f"No todos to update")
    # make updates
    try:
        for old_thing, new_thing in zip(matches, _update(matches, state=update_state, note=note)):
            # remove old versions
            todo_list.remove(old_thing)
            # add new versions
            todo_list.append(new_thing)
            # print summary
            print(f"Updated:\n{_str(new_thing)}\n")
    except RuntimeError as e:
        raise click.ClickException(e.args[0])
    # sort
    todo_list = shell.sort_(todo_list)
    # save
    shell.store_todos(todo_list)

    # print summary
    matches = _filter_list_by_state(todo_list, state=list_state)
    if matches:
        active_index = shell.load_active_index()
        current_active_index = min(active_index, len(matches) - 1)
        shell.store_active_index(current_active_index)
    else:
        print(f"No todos match the filter anymore: [{'|'.join(list_state)}]")
    print()
    _raw_stats()
    print()


def _get_completion_dates():
    # load todos
    import persistence
    shell.PERSISTENCE = persistence
    todo_list = shell.load_todos()
    # get completion dates
    dates = [t.crossed_out_at.date for t in todo_list if t.crossed_out_at]
    # formatted dates
    formatted_dates = list({f"{d.month}/{d.day}/{d.year}" for d in dates})
    return formatted_dates


def _to_real_dates(dates):
    real_dates = []
    for this_date in dates:
        month, day, year = [int(p) for p in this_date.split('/')]
        real_dates.append(entities.Date(year=year, month=month, day=day))
    return real_dates


@cli.command('done-on')
@click.argument('dates', nargs=-1, type=click.Choice(_get_completion_dates()))
def done_on(dates):
    # default date (not allowed with click with variadic args)
    if not dates:
        dates = (_get_completion_dates()[-1],)
    # to real dates
    dates = _to_real_dates(dates)
    # load todos
    todo_list = shell.load_todos()
    # filter them
    todos = use_cases.filter_to_completed_dates(todo_list, *dates)
    # error handling
    if not todos:
        raise click.ClickException("No todos match!")
    items = [(t.work, _state(t)) for t in todos]
    reported_state = ()
    print(f"Todos:")
    for summary, todo_state in items:
        if todo_state != reported_state:
            print(f"[{'|'.join((t for t in todo_state if t not in reported_state))}]")
            reported_state = todo_state
        print(f"  {summary}")
    print()


@cli.command()
def repl():
    prompt_kwargs = {
        'history': history.FileHistory(pathlib.Path('~/todo.history').expanduser()),
    }
    click_repl.repl(click.get_current_context(), prompt_kwargs=prompt_kwargs)



def register_state_aliases(group):
    all_states = _get_state_strings()
    def create_alias(name):
        @group.command(name)
        @click.argument('state', nargs=-1, type=click.Choice(all_states))
        @click.option('--note', default='')
        def alias(state, *, note):
            return _raw_update((name, *state), note=note)
    for this_state in all_states:
        create_alias(this_state)


def main():
    register_state_aliases(cli)
    cli()
