"""API for todos."""


# [ Imports ]
from __future__ import annotations
import datetime
import enum
import core
import use_cases
import entities


# [ Internals ]
def _sorting_key(todo: Todo):
    return (
        todo.completion_state.value,
        2 if todo.blocked_by_other else 1,
        _datetime_to_semantic_date(todo.due_date).value,
        todo.important.value,
        1 if todo.blocking_other else 2,
        todo.expected_timespan.value,
        todo.role.value,
        -todo.created_at.timestamp(),
        todo.scheduling.value,
    )


def _datetime_to_semantic_date(date_value):
    if isinstance(date_value, entities.Date):
        date_value = datetime.datetime(year=date_value.year, month=date_value.month, day=date_value.day)
    now = datetime.datetime.now()
    if date_value.date() < now.date():
        return SemanticDates['OVERDUE']
    if date_value.date() == now.date():
        return SemanticDates['TODAY']
    if date_value.date() == (now + datetime.timedelta(days=1)).date():
        return SemanticDates['TOMORROW']
    if date_value.date() <= (now + datetime.timedelta(days=7)).date():
        return SemanticDates['WITHIN_A_WEEK']
    if date_value.date() <= (now + datetime.timedelta(days=14)).date():
        return SemanticDates['WITHIN_TWO_WEEKS']
    if date_value.date() <= (now + datetime.timedelta(days=30)).date():
        return SemanticDates['WITHIN_A_MONTH']
    return SemanticDates['LATER']


def _semantic_date_to_datetime(semantic_date):
    now = datetime.datetime.now()
    if semantic_date is SemanticDates.OVERDUE:
        return now - datetime.timedelta(days=1)
    if semantic_date is SemanticDates.TODAY:
        return now
    if semantic_date is SemanticDates.TOMORROW:
        return now + datetime.timedelta(days=1)
    if semantic_date is SemanticDates.WITHIN_A_WEEK:
        return now + datetime.timedelta(days=7)
    if semantic_date is SemanticDates.WITHIN_TWO_WEEKS:
        return now + datetime.timedelta(days=14)
    if semantic_date is SemanticDates.WITHIN_A_MONTH:
        return now + datetime.timedelta(days=30)
    return now + datetime.timedelta(days=60)


def _if_not(thing, *, default):
    if not thing:
        return default
    return thing


# [ API:Data ]
Timespans = core.Timespans
CompletionState = core.CompletionState
Roles = core.Roles
Todo = core.Todo
Scheduling = core.Scheduling


class SemanticDates(enum.Enum):
    OVERDUE = 1
    TODAY = 2
    TOMORROW = 3
    WITHIN_A_WEEK = 4
    WITHIN_TWO_WEEKS = 5
    WITHIN_A_MONTH = 6
    LATER = 7


# XXX get rid of this - pass around shell state to functions?
PERSISTENCE=None


# [ API:Functions ]
def set_persistence(persistence):
    global PERSISTENCE
    PERSISTENCE = persistence


def cancel_todo(todo, *, cancellation_reason):
    return core.cancel_todo(
        todo,
        cancellation_reason=cancellation_reason,
        cancellation_time=datetime.datetime.now(),
    )


def load_todos(location_string="~/todos.toml"):
    todos = PERSISTENCE.load_todos(location_string)
    todos = core.reschedule_upcoming_todos(todos, now=datetime.datetime.now())
    todos = sort_(todos)
    return todos


def store_todos(todos, *, location_string="~/todos.toml"):
    PERSISTENCE.store_todos(todos, location_string=location_string)


def load_active_filter(location_string="~/todos-filter.txt"):
    return PERSISTENCE.load_active_filter(location_string)


def store_active_filter(filter_state, *, location_string="~/todos-filter.txt"):
    return PERSISTENCE.store_active_filter(filter_state, location_string=location_string)


def load_active_index(location_string="~/todos-index.txt"):
    return PERSISTENCE.load_active_index(location_string)


def store_active_index(index, *, location_string="~/todos-index.txt"):
    return PERSISTENCE.store_active_index(index, location_string=location_string)


def build_new_todo(
    summary, *,
    important,
    due_date,
    blocked_by_other,
    blocking_other,
    role,
    expected_timespan,
    scheduling,
):
    return core.build_new_todo(
        summary,
        important=important,
        due_date=_semantic_date_to_datetime(due_date),
        blocked_by_other=blocked_by_other,
        blocking_other=blocking_other,
        role=role,
        expected_timespan=expected_timespan,
        scheduling=scheduling,
    )


def sort_(todos):
    return sorted(todos, key=_sorting_key)


def add_to_list(todo):
    # load existing todos
    todo_list = load_todos()
    # add the new one
    todo_list = (*todo_list, todo)
    # sort them all
    todo_list = sort_(todo_list)
    # save
    store_todos(todo_list)


def filter_list(
    todo_list, *,
    snippet='',
    completion_states=tuple(core.CompletionState),
    timespans=tuple(core.Timespans),
    blocking_states=(True, False),
    blocked_states=(True, False),
    importance_states=(True, False),
    due_dates=tuple(SemanticDates),
    schedules=tuple(core.Scheduling),
):
    todo_list = [t for t in todo_list if snippet.lower() in t.work.lower()]
    todo_list = [t for t in todo_list if t.completion_state in completion_states]
    todo_list = [t for t in todo_list if t.expected_timespan in timespans]
    todo_list = [t for t in todo_list if t.blocking_other in blocking_states]
    todo_list = [t for t in todo_list if t.blocked_by_other in blocked_states]
    todo_list = [t for t in todo_list if t.important in importance_states]
    todo_list = [t for t in todo_list if _datetime_to_semantic_date(t.due_date) in due_dates]
    todo_list = [t for t in todo_list if t.scheduling in schedules]
    return todo_list


def update_list(
    todo_list, *,
    timespan,
    blocking_state,
    blocked_state,
    importance_state,
    role,
    due_date,
    scheduling,
    completion_state,
    note,
    weekday,
):
    for old_todo in todo_list:
        temp_todo = old_todo
        temp_todo = _if_not(core._apply_unless_none(
            timespan, func=lambda a: temp_todo.copy_with(expected_timespan=a),
        ), default=temp_todo)
        # pipe might help a little?
        # temp_todo = core._apply_unless_none(
        #     timespan,
        #     func=lambda a: temp_todo.copy_with(expected_timespan=a),
        # ) | _if_not(default=temp_todo)

        temp_todo = _if_not(core._apply_unless_none(timespan, func=lambda a: temp_todo.copy_with(expected_timespan=a)), default=temp_todo)
        temp_todo = _if_not(core._apply_unless_none(blocking_state, func=lambda a: temp_todo.copy_with(blocking_other=a)), default=temp_todo)
        temp_todo = _if_not(core._apply_unless_none(blocked_state, func=lambda a: temp_todo.copy_with(blocked_by_other=a)), default=temp_todo)
        temp_todo = _if_not(core._apply_unless_none(importance_state, func=lambda a: temp_todo.copy_with(important=a)), default=temp_todo)
        temp_todo = _if_not(core._apply_unless_none(role, func=lambda a: temp_todo.copy_with(role=a)), default=temp_todo)
        temp_todo = _if_not(core._apply_unless_none(due_date, func=lambda a: temp_todo.copy_with(due_date=_semantic_date_to_datetime(a))), default=temp_todo)
        temp_todo = _if_not(core._apply_unless_none(scheduling, func=lambda a: temp_todo.copy_with(scheduling=scheduling)), default=temp_todo)
        temp_todo = _if_not(core._apply_unless_none(weekday, func=lambda w: use_cases.reschedule_to_weekday(temp_todo, weekday=weekday)), default=temp_todo)
        temp_todo = core.update_completion_state(temp_todo, completion_state=completion_state, now=datetime.datetime.now(), cancellation_reason=note)
        new_todo = temp_todo
        yield new_todo
