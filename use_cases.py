"""Todo use cases - what we want to do with todos."""


# [ Imports ]
import steps
import entities


# [ API ]
# trying out a simplified Clean Architecture
# * core contains use cases -> steps-to-implement-them & data-required
# * primary adapters call these use cases with the core data types
# * secondary adapters are called by the steps with the core data types
# * use cases are what anyone is going to want to do with the core data types,
#   they are the most basic correct uses/actions on the data, and
#   farming them out to a plugin would fundamentally alter what the core data
#   or actions are about, or be nonsensical, because there's only one valid way.

# BASIC ACTIONS
# XXX create
complete_todo = steps.complete_todo
# XXX cancel
# XXX update summary
# XXX re-open
# XXX update due date
# XXX update role
# XXX update blocking
# XXX update blocked
# XXX update duration
# XXX update scheduling
# XXX update priority

# These don't belong here - move them out.
# FILTERING
# XXX filter to completion-states
# XXX filter to summary
# XXX filter to due date
filter_to_roles = steps.filter_to_roles
# XXX filter to blocking
# XXX filter to blocked
# XXX filter to duration
# XXX filter to scheduling
# XXX filter to priority
filter_to_completed_dates = steps.filter_to_completed_dates

# SORTING
# XXX sort by attribute order and inc/dec per attr

# ADVANCED ACTIONS
def reschedule_to_weekday(todo, *, weekday):
    """Reschedules the given todo to the given weekday."""
    steps.validate_weekday(weekday)
    date = steps.translate_weekday_into_date(weekday)
    rescheduled_todo = steps.update_due_date(todo, due_date=date)
    return rescheduled_todo
