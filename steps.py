"""Todo use-case steps."""


# [ Imports ]
import entities
import secondaries


# [ Internals ]
def _get_datetime():
    return secondaries.dates.get_datetime()


def _get_days_till_weekday(today: entities.Date, *, weekday: entities.Weekdays):
    today_weekday = secondaries.dates.get_weekday(today)
    days_till_weekday = weekday.value - today_weekday.value
    if days_till_weekday <= 0:
        days_till_weekday += 7
    return days_till_weekday


# [ API ]
def validate_weekday(weekday):
    """Raise an exception if the weekday given is not a Weekday enum."""
    if not isinstance(weekday, entities.Weekdays):
        raise TypeError(f"An invalid type was passed as a weekday.  Got {type(weekday)}.  Expected Weekday.")


def translate_weekday_into_date(weekday):
    """Translate the given weekday into a date."""
    today = secondaries.dates.get_date()
    days_till_weekday = _get_days_till_weekday(today, weekday=weekday)
    target_date = secondaries.dates.add_days(today, days=days_till_weekday)
    return target_date


def update_due_date(todo, *, due_date):
    return todo.copy_with(due_date=due_date, completion_state=entities.CompletionStatus.OPEN)


def complete_todo(todo):
    """
    Complete the todo.

    This sets the completion state to COMPLETE, and
    sets the completion time to right now.
    """
    return todo.copy_with(
        completion_state=entities.CompletionStatus.DONE,
        crossed_out_at=_get_datetime(),
        blocked_by_other=False,
    )


def filter_to_completed_dates(todos, *completion_dates):
    """Filter down to the items completed on the given dates."""
    return (t for t in todos if t.crossed_out_at and t.crossed_out_at.date in completion_dates)


def filter_to_roles(todos, *roles):
    """Include only the given roles."""
    return (t for t in todos if t.role in roles)
