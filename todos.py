#! /usr/bin/env python


"""
scheduling:
    add abc daily|weekly
    update abc once|daily|weekly
subtasks:
    add abc some-label
    update abc not-other-label some-label
    add abc --labels some-label
    update abc not-other-label --labels some-label some-other-label
    list some-label

scheduling something means that if it's in completed, and more than the scheduled time has passed,
a new copy will get put back into rotation.

subtasks can be completed individually - they're just tasks
parent tasks cannot be modified directly - they're just labels

new labels must be added with an explicit flag
labels can't start with "not_"
labels can't be the same as state variables
new labels can be the same as existing labels

search/find/list all become the same
active search is the last search
active item clears out on a search
next/previous/first operate on the active search list
operations are applied to all items in the active search if there is no active item
operations applied to a search rather than an active item have a confirmation prompt with them.

undo

reason for blocking

backup:
* git commit every change

urgency:
* overdue - sets due date of yesterday - applied to past due dates
* today - sets due date of today - applied to today due dates
* tomorrow - sets due date of tomorrow - applied to tomorrow due dates
* this week - sets due date of upcoming friday - applied if due date is btw tomorrow & friday
* next week - sets due date of the following friday - applied if due date is btw friday & next
* later - unsets due date - applied if no due date

importance:
* 0 - must get done.
* 1 - should get done.
* 2 - polish/nice-to-have.
* 3 - interesting idea.
"""

# [ Imports ]
# adapters
# adapters:mixed
import ui
# adapters:secondary
import persistence
# shell
import shell


# [ Script ]
# connect adapters
shell.set_persistence(persistence)
# launch
ui.main()
