import enum
import din
import decimal


# [ API:Supporting ]
class Date(din.EqualityMixin, din.ReprMixin):
    def __init__(self, *, year: int, month: int, day: int):
        super().__init__()
        self.year = year
        self.month = month
        self.day = day


class Importance(enum.Enum):
    OVERRIDING = 1  # overriding importance
    MUST = 2  # must get done
    SHOULD = 3  # should get done, but not necessary
    WOULD_LIKE = 4  # would like to get this done, but no imperative
    IDEA = 5  # unclear whether this is important


class Roles(enum.Enum):
    DIRECTOR = 1
    QE_LEAD = 2
    AI_MANAGER = 3
    DEVOPS_MANAGER = 4
    QE_CONTRIBUTOR = 5
    PERSONAL = 6


class Intervals(enum.Enum):
    ONCE = 1
    DAILY = 2
    WEEKLY = 3
    MONTHLY = 4
    QUARTERLY = 5
    ANNUALLY = 6


class Duration(din.EqualityMixin, din.ReprMixin):
    def __init__(
        self, *,
        years: int = 0,
        months: int = 0,
        weeks: int = 0,
        days: int = 0,
        hours: int = 0,
        minutes: int = 0,
        seconds: decimal.Decimal = decimal.Decimal(0),
    ) -> None:
        super().__init__()
        if not (0 <= years):
            raise ValueError("Years must be >= 0")
        self.years = years
        if not (0 <= months <= 11):
            raise ValueError("Months must be between [0, 11]")
        self.months = months
        if not (0 <= weeks <= 3):
            raise ValueError("Weeks must be between [0, 3]")
        self.weeks = weeks
        if not (0 <= days <= 6):
            raise ValueError("Days must be between [0, 6]")
        self.days = days
        if not (0 <= hours <= 23):
            raise ValueError("Hours must be between [0, 23]")
        self.hours = hours
        if not (0 <= minutes <= 59):
            raise ValueError("Minutes must be between [0, 59]")
        self.minutes = minutes
        if not (0 <= seconds <= 59):
            raise ValueError("Seconds must be between [0, 59]")
        self.seconds = seconds


class CompletionStatus(enum.Enum):
    OPEN = 1
    DONE = 2
    CANCELLED = 3


class CompletionState(din.EqualityMixin, din.ReprMixin):
    def __init__(self, status: CompletionStatus, *, note: str) -> None:
        super().__init__()
        self.status = status
        self.note = note


# [ API:Core ]
class Todo(din.EqualityMixin, din.ReprMixin):
    def __init__(
        self,
        summary: str, *,
        due_date: Date,
        importance: Importance = Importance.SHOULD,
        blocking: str = '',
        blocked_by: str = '',
        role_associated: Roles = Roles.PERSONAL,
        interval: Intervals = Intervals.ONCE,
        expected_time_required: Duration = Duration(minutes=30),
    ) -> None:
        super().__init__()
        self.summary = summary
        self.completion_history: typing.Tuple[CompletionState, ...] = (
            CompletionState(CompletionStatus.OPEN, note=''),
        )
        self.due_date = due_date
        self.importance = importance
        self.blocking = blocking
        self.blocked_by = blocked_by
        self.role_associated = role_associated
        self.interval = interval
        self.expected_time_required = expected_time_required


class Weekdays(enum.Enum):
    SUNDAY    = 0
    MONDAY    = 1
    TUESDAY   = 2
    WEDNESDAY = 3
    THURSDAY  = 4
    FRIDAY    = 5
    SATURDAY  = 6


class Time(din.EqualityMixin, din.ReprMixin):
    def __init__(self, *, hour: int, minute: int, second: int):
        super().__init__()
        self.hour = hour
        self.minute = minute
        self.second = second


class DateTime(din.EqualityMixin, din.ReprMixin):
    def __init__(self, *, date: Date, time: Time):
        super().__init__()
        self.date = date
        self.time = time
