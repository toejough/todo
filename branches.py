"""Functions for branching logic shortcuts."""


# [ Imports ]
import pipe


# [ API ]
def do_if_none(value, *, func):
    if value is None:
        return func()
    return value


def apply_if(value, *, predicate, func):
    if predicate(value):
        return func(value)
    return value


def apply_unless_none(value, *, func):
    if value is None:
        return value
    return func(value)


def if_not(value, *, default):
    if not value:
        return default
    return value


p_if_not = pipe.Pipe(if_not)
